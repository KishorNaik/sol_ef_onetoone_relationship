﻿using Sol_EF_OneToOne_Relationship.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sol_EF_OneToOne_Relationship
{
    class Program
    {
        static void Main(string[] args)
        {
            Task.Run(async () => {

                IEnumerable<UserEntity> userEntityObj =await new UserDal().GetUserData();

            }).Wait();
        }
    }
}
